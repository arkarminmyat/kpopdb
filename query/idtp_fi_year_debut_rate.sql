-- idtp_fi_year_debut_rate
CREATE TABLE 'idtp_fi_year_debut_rate' AS
SELECT TMP,Gp_Type,COUNT(*) AS Debut_Rate FROM(
SELECT Name,Debut,CAST(substr(Debut,0,5) AS INTEGER) AS TMP,Gp_Type
FROM idtm_gp_details)
GROUP BY TMP,Gp_Type

COMMIT;