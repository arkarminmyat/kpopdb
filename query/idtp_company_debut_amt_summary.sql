-- idtp_company_debut_amt_summary
CREATE TABLE 'idtp_company_debut_amt_summary' AS 
SELECT Company,COUNT(*) AS Debut_Amt FROM
(SELECT Company,CAST(substr(Debut,0,5) AS INTEGER) AS Debut_Year
FROM idtm_gp_details)
GROUP BY Company

COMMIT;