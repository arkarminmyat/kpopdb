-- idtp_company_debut_amt_detail
CREATE TABLE 'idtp_company_debut_amt_detail' AS 
SELECT Debut_Year,Company,COUNT(*) AS Debut_Amt FROM
(SELECT Company,CAST(substr(Debut,0,5) AS INTEGER) AS Debut_Year
FROM idtm_gp_details)
GROUP BY Debut_Year,Company

COMMIT;