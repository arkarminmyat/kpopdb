-- idtv_gene
CREATE TABLE 'idtv_gene' AS
SELECT Name,
	   Debut,
	   Service_Year,
	   Gp_Type,
	   CASE WHEN (tmp >= 1996) AND (tmp <= 2004) THEN '1st GENERATION'
	   WHEN (tmp >= 1996) AND (tmp <= 2004) THEN '1st GENERATION'
	   WHEN (tmp >= 2005) AND (tmp <= 2011) THEN '2nd GENERATION'
	   WHEN (tmp >= 2012) AND (tmp <= 2017) THEN '3rd GENERATION'
	   WHEN (tmp >= 2018) THEN '4th GENERATION'
	   END AS Gene_Typ
FROM(
SELECT Name,Debut,Service_Year,Gp_Type,CAST(substr(Debut,0,5) AS INTEGER) AS TMP FROM
(SELECT Name,KoreanName,Debut,date('now') - Debut AS Service_Year,Gp_Type
FROM idtm_gp_details))

COMMIT;