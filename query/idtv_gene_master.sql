-- idtv_gene_master
CREATE TABLE 'idtv_gene_master' AS
SELECT Name,Debut,Service_Year,
	   CASE 
	   WHEN Gp_Type = 'FEMALE' AND Gene_Typ = '0th GENERATION' THEN '0G_F_GP'
	   WHEN Gp_Type = 'MALE' AND Gene_Typ = '0th GENERATION' THEN '0G_M_GP'
	   WHEN Gp_Type = 'FEMALE' AND Gene_Typ = '1st GENERATION' THEN '1G_F_GP'
	   WHEN Gp_Type = 'MALE' AND Gene_Typ = '1st GENERATION' THEN '1G_M_GP'
	   WHEN Gp_Type = 'FEMALE' AND Gene_Typ = '2nd GENERATION' THEN '2G_F_GP'
	   WHEN Gp_Type = 'MALE' AND Gene_Typ = '2nd GENERATION' THEN '2G_M_GP'
	   WHEN Gp_Type = 'FEMALE' AND Gene_Typ = '3rd GENERATION' THEN '3G_F_GP'
	   WHEN Gp_Type = 'MALE' AND Gene_Typ = '3rd GENERATION' THEN '3G_M_GP'
	   WHEN Gp_Type = 'FEMALE' AND Gene_Typ = '4th GENERATION' THEN '4G_F_GP'
	   WHEN Gp_Type = 'MALE' AND Gene_Typ = '4th GENERATION' THEN '4G_M_GP'
	   END AS Gene_Gender_Class
FROM
(SELECT Name,
	   Debut,
	   Service_Year,
	   Gp_Type,
	   CASE WHEN (tmp >= 1996) AND (tmp <= 2004) THEN '1st GENERATION'
	   WHEN (tmp >= 1996) AND (tmp <= 2004) THEN '1st GENERATION'
	   WHEN (tmp >= 2005) AND (tmp <= 2011) THEN '2nd GENERATION'
	   WHEN (tmp >= 2012) AND (tmp <= 2017) THEN '3rd GENERATION'
	   WHEN (tmp >= 2018) THEN '4th GENERATION'
	   ELSE '0th GENERATION'
	   END AS Gene_Typ
FROM(
SELECT Name,Debut,Service_Year,Gp_Type,CAST(substr(Debut,0,5) AS INTEGER) AS TMP FROM
(SELECT Name,KoreanName,Debut,date('now') - Debut AS Service_Year,Gp_Type
FROM idtm_gp_details)))

COMMIT;
