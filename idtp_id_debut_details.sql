-- idtp_id_debut_details
CREATE TABLE 'idtp_id_debut_details' AS
SELECT StageName,FullName,DateofBirth,Age,Debut,Service_Year,
	   CASE WHEN GP_Name IS NULL THEN 'VA'
	   ELSE GP_Name END AS GP_Name
FROM
(SELECT StageName,FullName,DateofBirth,Age,
	CASE WHEN Name is NULL THEN GroupName
	ELSE Name END AS GP_Name,
	Debut,Service_Year
	FROM
(SELECT A.StageName,A.FullName,A.DateofBirth,A.Age,A.GroupName,B.Name,B.Debut,B.Service_Year FROM
(SELECT StageName,FullName,DateofBirth,date('now') - DateofBirth AS Age,GroupName
FROM idtm_details) A
LEFT JOIN
(SELECT Name,Debut,date('now') - Debut AS Service_Year 
FROM idtm_gp_details) B
ON A.GroupName = B.Name)
ORDER BY StageName)

COMMIT;
